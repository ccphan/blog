class Post < ActiveRecord::Base
	# add the associations/relationships
        # we can have the following:
	#	one-to-one: 	has_one <-> belongs_to
	#	one-to-many:	has_many <-> belongs_to
	#	many-to-many:	has_and_belongs_to_many <-> *
	
	has_many :comments, dependent: :destroy

	# validation hooks
	validates_presence_of	:title
	validates_presence_of	:body
	
end
