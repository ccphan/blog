class Comment < ActiveRecord::Base
	# add association
	belongs_to :post

	validates_presence_of	:post_id
	validates_presence_of	:body
end
